<?php

for ($i = 0; $i < 10; $i++) {
    $numbers[] = rand(1, 20);
}

$result = $numbers;

mergeSort($numbers, $result);

function mergeSort(array $numbers, array &$result)
{
    echo "Original array: ";
    echo implode(', ', $numbers) . "\n";

    split($numbers, 0, count($numbers), $result);

    echo "Sorted array: ";
    echo implode(', ', $result) . "\n";
}

function split(array $numbers, int $indexBegin, int $indexEnd, array &$result)
{
    if ($indexEnd - $indexBegin <= 1) return;

    $indexMiddle = intval(($indexBegin + $indexEnd) / 2);

    split($result, $indexBegin, $indexMiddle, $numbers);
    split($result, $indexMiddle, $indexEnd, $numbers);

    merge($numbers, $indexBegin, $indexMiddle, $indexEnd, $result);
}

function merge(array $numbers, int $indexBegin, int $indexMiddle, int $indexEnd, array &$result)
{
    $i = $indexBegin;
    $j = $indexMiddle;

    for ($k = $indexBegin; $k < $indexEnd; $k++) {
        if ($i < $indexMiddle && ($j >= $indexEnd || $numbers[$i] <= $numbers[$j])) {
            $result[$k] = $numbers[$i];
            $i = $i + 1;
        } else {
            $result[$k] = $numbers[$j];
            $j = $j + 1;
        }
    }
}
